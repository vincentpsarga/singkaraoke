This is a sample project designed to show HipTest how the Aeyrium team would like to see the output from HipTest.

The basic project structure is very similar with one addition, a folder called `actionwords` in the root of the project which is created by the developer (not generated by `hiptestpublisher`).

> NOTE: We are using CucumberJS v3.0.1 which has incompatible syntax differences from previous versions which is necessitating some of the changes being proposed.

# Project Structure 
```
KaraokeProject
 |
 +- actionwords
 |  |
 |  +- iCanSingTheSong.js
 |  +- iSeeTheLyrics.js
 |  +- iSelectTheSongSongName.js
 +- features
 |  |
 |  +- step_definitions
 |  |   |
 |  |   +- actionwords.js
 |  |   +- step_definitions.js
 |  |
 |  +- karaoke.feature
 |
 +- node_modules
 +- hiptest-publisher.conf
 +- package.json
 +- ReadMe.md // this file
 
```

# Sing_Karaoke.feature
```gerkin 
Feature: Sing Karaoke
    Verify that I can turn on the karaoke machine and sing.

  Scenario: Singer sings the lyrics to song
    Given I select the song "Unchain My Heart"
    And I see the lyrics
    Then I can sing the song

```

# Changes to the step_definitions.js File
```javascript
// NOTE: This file has been changes to support the new syntax
// requirements of CucumberJS v3.0.1.  The current output format
// generated by HipTest CLI is no longer supported.

var {defineSupportCode} = require('cucumber');

defineSupportCode(function({Then, When, Given, Before, After}) {
    Before(function (scenario) {
        this.actionwords = Object.create(require('./actionwords.js').Actionwords);
    });

    After(function (scenario) {
        this.actionwords = null;
    });


    Given('I select the song {string}', function (song_name, callback) {
        this.actionwords.iSelectTheSongSongName(song_name);
        callback();
    });

    Given('I see the lyrics', function (callback) {
        this.actionwords.iSeeTheLyrics();
        callback();
    });

    Then('I can sing the song', function (callback) {
        this.actionwords.iCanSingTheSong();
        callback();
    });
});

```

# Changes to the actionwords.js File
```javascript
// NOTE: In this file we are simply moving the function definitions into their own files
// which are contained outside of the HipTest CLI generated code in the directory
// {project_home}/actionswords/*.js
//
// The purpose of doing so is as follows:
// 1. No need to perform complex merge and diffs of the actionwords.js file.  
// 2. No project code will ever be merged in with generated code from HipTest
// 3. Missing action word implementations will be fast and easy to find simply by running
//    cucumberjs and looking for the failed "require" calls.  

exports.Actionwords = {
    iSelectTheSongSongName: require("../../actionwords/iSelectTheSongSongName.js"),
    iSeeTheLyrics: require("../../actionwords/iSeeTheLyrics.js"),
    iCanSingTheSong: require("../../actionwords/iCanSingTheSong.js"),
};
```

All of the above code is run by simply typing the following command ...

```
bash-3.2$ ./node_modules/cucumber/bin/cucumber.js
```

The output should look something like the following...
```
.Song set to `Unchain My Heart`
.I see the lyrics for `Unchain My Heart`
.I am singing `Unchain My Heart`
..

1 scenario (1 passed)
3 steps (3 passed)
0m00.005s
```