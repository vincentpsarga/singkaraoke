// NOTE: In this file we are simply moving the function definitions into their own files
// which are contained outside of the HipTest CLI generated code in the directory
// <project_home>/actionswords/*.js
//
// The purpose of doing so is as follows:
// 1. No need to perform complex merge and diffs of the actionwords.js file.
// 2. No project code will ever be merged in with generated code from HipTest
// 3. Missing action word implementations will be fast and easy to find simply by running
//    cucumberjs and looking for the failed "require" calls.

exports.Actionwords = {
    iSelectTheSongSongName: require("../../actionwords/iSelectTheSongSongName.js"),
    iSeeTheLyrics: require("../../actionwords/iSeeTheLyrics.js"),
    iCanSingTheSong: require("../../actionwords/iCanSingTheSong.js"),
}
