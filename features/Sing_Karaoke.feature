Feature: Sing Karaoke
    Verify that I can turn on the karaoke machine and sing.

  Scenario: Singer sings the lyrics to song
    Given I select the song "Unchain My Heart"
    And I see the lyrics
    Then I can sing the song
